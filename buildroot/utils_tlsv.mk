################################################################################
#
# utils_tlsv
#
################################################################################

UTILS_TLSV_VERSION = master
UTILS_TLSV_SITE = https://gitlab.com/hellodong/util_tlsrv/-/archive/${UTILS_TLSV_VERSION}
UTILS_TLSV_LICENSE = GPL-2.0
UTILS_TLSV_INSTALL_STAGING = YES

define UTILS_TLSV_INSTALL_INIT_SYSV
	$(INSTALL) -m 0755 -D package/utils_tlsv/S96utils_tlsv \
	$(TARGET_DIR)/etc/init.d/S96utils_tlsv
endef

$(eval $(cmake-package))
