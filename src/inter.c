

#include <string.h>
#include <stdio.h>

#include "inter.h"
#include "cmd_line_parse.h"


#define LINE_MAX_SIZE   4096

typedef enum
{
    INPUT_ENTER,
    INPUT_BACKSPACE,
    INPUT_LEFT,
    INPUT_RIGHT,
    INPUT_UP,
    INPUT_DOWN,
    INPUT_DEL,
    INPUT_TAB,
}inter_input_e;

typedef struct {
    char start_name[128];
    char cur_line[LINE_MAX_SIZE];   
    uint32_t len;
    uint32_t cursor;
}stOneLine_t;

typedef int (*Cmd_handle_t)(uint8_t in_out[], uint32_t input_len, uint32_t out_size, stOneLine_t *one_line);

typedef struct
{
    const char *cmd_str;
    uint16_t cmd_len;
    inter_input_e input_type;
    Cmd_handle_t cmd_handle;
}stCmdTypeHandle_t;

stOneLine_t g_one_line;

int _enter_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line);
int _backspace_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line);
int _left_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line);
int _right_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line);
int _down_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line);
int _up_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line);
int _del_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line);
int _tab_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line);
int _default_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line);


stCmdTypeHandle_t g_cmds_handle[] = {
    {"\b",     1, INPUT_BACKSPACE,    _backspace_handle},
    {"\t",     1, INPUT_TAB,          _tab_handle},
    {"\r",     1, INPUT_ENTER,        _enter_handle},
    {"\x1b[D", 3, INPUT_LEFT,         _left_handle},
    {"\x1b[C", 3, INPUT_RIGHT,        _right_handle},
    {"\x1b[A", 3, INPUT_UP,           _up_handle},
    {"\x1b[B", 3, INPUT_DOWN,         _down_handle},
};

Cmd_handle_t _input_cmd_lookup(uint8_t in[], uint32_t in_len)
{
	int idx = 0;

	switch (in[0])
	{
		case '\b':
		case '\t':
		case '\r':
			for(idx = 0; idx < COUNTOF(g_cmds_handle);idx++)
			{
				if (in[0] == g_cmds_handle[idx].cmd_str[0])
				{
					return g_cmds_handle[idx].cmd_handle;
				}
			}
		break;

		case '\x1b':
			for(idx = 0; idx < COUNTOF(g_cmds_handle);idx++)
			{
				if (in_len >= g_cmds_handle[idx].cmd_len 
				    &&0 == memcmp(in, g_cmds_handle[idx].cmd_str, g_cmds_handle[idx].cmd_len))
				{
					return g_cmds_handle[idx].cmd_handle;
				}
			}
		break;
	}
	return NULL;
}

#if 0
int __echo_process(char input_str[], uint8_t _out[], uint32_t out_size)
{
    int rt = 0;
    if (strlen(input_str))
    {
        strncpy((char *)_out,input_str, out_size);
        rt = strlen((char *)_out);
    }
    return rt;
}
#endif

int inter_init(uint8_t in_out[], uint32_t out_size, const char start_name[])
{
    g_one_line.len = 0;
    g_one_line.cursor = 0;
    memset(g_one_line.cur_line, 0x00, LINE_MAX_SIZE);
    strncpy(g_one_line.start_name, start_name, COUNTOF(g_one_line.start_name));

    return snprintf((char *)in_out, out_size, "\r\n%s> ", g_one_line.start_name);
}

int inter_lineinput(uint8_t in_out[], uint32_t input_len, uint32_t out_size)
{
    Cmd_handle_t _method = NULL;

    _method = _input_cmd_lookup(in_out, input_len);    
    if (_method)
    {
        return _method(in_out, input_len, out_size, &g_one_line);
    }

    return _default_handle(in_out, input_len, out_size, &g_one_line);
}

int _enter_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line)
{
    int rt = 0;

    one_line->cur_line[one_line->len] = '\0';
    //hanle input command
    rt = _cmd_line_process(one_line->cur_line, in_out + 2, out_size -2);
    //hanle done

    one_line->len = 0;
    one_line->cursor = 0; 

    if (rt > 0)
    {
        in_out[0] = '\r';
        in_out[1] = '\n';
        rt += 2;
        rt += snprintf((char *)in_out + rt, out_size - rt,"\r\n%s> ", one_line->start_name);
    }
    else if (rt == 0)
    {
	printf("normal exit\n");
    }
    else
    {
        rt = snprintf((char *)in_out, out_size, "\r\n%s> ", one_line->start_name);
    }
    
    return rt;
}

int _backspace_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line)
{
    int out_len = 0;
    if (one_line->len > 0)
    {
        out_len = snprintf((char *)in_out, out_size, "\b \b");
        one_line->len--;
        one_line->cursor--;
    }

    return out_len;
}

int _left_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line)
{
    /* TODO */
    return 0;
}

int _right_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line)
{
    /* TODO */
    return 0;
}

int _down_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line)
{
    /* TODO */
    return 0;
}

int _up_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line)
{
    /* TODO */
    return 0;
}

int _del_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line)
{
    /* TODO */
    return 0;
}
int _tab_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line)
{
    /* TODO */
    return 0;
}

int _default_handle(uint8_t in_out[], uint32_t in_len, uint32_t out_size, stOneLine_t *one_line)
{
    if (in_len > 1)
    {
        memcpy(one_line->cur_line + one_line->len, in_out, in_len);
    }
    else
    {
        one_line->cur_line[one_line->len] = (char)in_out[0];
    }

    one_line->cursor += in_len;
    one_line->len += in_len;

    return in_len;
}
