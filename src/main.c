

#include <stdio.h>  
#include <stdlib.h>  
#include <string.h>  
#include <errno.h>  
#include <unistd.h>
#include <sys/types.h>  
#include <sys/socket.h>  
#include <netinet/in.h>  
#include <poll.h>

#include "git_ver.h"
#include "inter.h"


#define DEFAULT_PORT 2222
#define MAXLINE 4096  

enum {
    _T_ECHO = 1,
    _T_GA=3,
    _T_WILL = 0xfb,
    _T_WONT,
    _T_DO,
    _T_DONT,
    _T_CMD
};


const char g_start_name[] = "purefarmer";

const unsigned char __tel_will_ga[] = {_T_CMD,_T_WILL,_T_GA};
const unsigned char __tel_wont_ga[] = {_T_CMD,_T_WONT,_T_GA};
const unsigned char __tel_will_echo[] = {_T_CMD,_T_WILL,_T_ECHO};
const unsigned char __tel_wont_echo[] = {_T_CMD,_T_WONT,_T_ECHO};
const unsigned char __tel_dont_echo[] = {_T_CMD,_T_DONT,_T_ECHO};

static void _send_git_info(int connect_fd);


void telnetd_cmd(int connected_fd)
{
    int rt = 0;
    unsigned char data = 0;
    int timeout= 0;
    struct pollfd poll_fds[1] ;

    if (write(connected_fd, __tel_will_ga, sizeof(__tel_will_ga)) < 0 
            || write(connected_fd, __tel_will_echo, sizeof(__tel_will_echo)) < 0
            || write(connected_fd, __tel_dont_echo, sizeof(__tel_dont_echo)) < 0
       ) 
    {
        printf("set error\n");
        return;
    }

    poll_fds[0].fd = connected_fd;
    poll_fds[0].events = POLLIN | POLLPRI;
    timeout = 50;

    while(1)
    {
        rt = poll(poll_fds, 1, timeout);	
        if (rt == 0)
        {
            printf("poll time out\n");
            break;
        }
        else if (rt < 0)
        {
            printf("poll error.\n");
            break;
        }
        else
        {

            if (recv(connected_fd, &data, 1, 0) != 1)
            {
                printf("poll done.\n");
                break;
            }

            if (_T_CMD == data)
            {
                unsigned char cmd[8];

                recv(connected_fd, cmd + 1, 2,0);
                printf(" 0x%02x  0x%02x\n", cmd[1], cmd[2]);
                switch (cmd[1])
                {
                    case _T_WILL:
                    case _T_DO:
                        cmd[1] = _T_DO;
                        break;
                    case _T_WONT:
                    case _T_DONT:
                        cmd[1] = _T_DONT;
                        break;
                }
                cmd[0] = data;
                send(connected_fd, cmd, 3,0);
            }
        }
    }
    return ;
}

int main(int argc, char** argv)  
{  
    int rt = 0;
    int    socket_fd, connect_fd;  
    struct sockaddr_in     servaddr;  
    //初始化Socket  
    if( (socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1 ){  
        printf("create socket error: %s(errno: %d)\n",strerror(errno),errno);  
        return -1;
    }  
    //初始化  
    memset(&servaddr, 0, sizeof(servaddr));  
    servaddr.sin_family = AF_INET;  
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);//IP地址设置成INADDR_ANY,让系统自动获取本机的IP地址。  
    servaddr.sin_port = htons(DEFAULT_PORT);//设置的端口为DEFAULT_PORT  

    int optval = 1;
    setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    //将本地地址绑定到所创建的套接字上  
    if( bind(socket_fd, (struct sockaddr*)&servaddr, sizeof(servaddr)) == -1){  
        printf("bind socket error: %s(errno: %d)\n",strerror(errno),errno);  
        return -1;
    }  

    printf("socket bind to port %d\n", DEFAULT_PORT);
    //开始监听是否有客户端连接  
    if( listen(socket_fd, 1) == -1){  
        printf("listen socket error: %s(errno: %d)\n",strerror(errno),errno);  
        return -1;
    }  

    connect_fd = 0;
    while(1)
    {  
        unsigned char buff[MAXLINE];  
        if(0 == connect_fd && (connect_fd = accept(socket_fd, (struct sockaddr*) NULL, NULL)) == -1)
        {  
            printf("accept socket error: %s(errno: %d)",strerror(errno),errno);  
            continue;  
        }  

        telnetd_cmd(connect_fd);

        _send_git_info(connect_fd);

        rt = inter_init(buff, COUNTOF(buff), g_start_name);
        if (rt > 0)
        {
            write(connect_fd, buff, rt);
        }

        while (1)
        {
            rt = recv(connect_fd, buff , MAXLINE, 0);
            printf("%d\n", rt);
            if (rt < 0)
            {
                printf("recv error %d\n", rt);
                break;
            }
            else if (rt == 0)
            {
                printf("connect fd closed.\n");
                break;
            }
            else
            {
                rt = inter_lineinput(buff, rt, COUNTOF(buff));
                if (rt > 0)
                {
                    write(connect_fd, buff, rt);
                }
		else if (0 == rt)
                {
			printf("will close this connection\n");
			break;
		}
                else if (rt < 0)
                {
                    printf("close this session\n");
                    break;
                }
            }
        }
        close(connect_fd);
        connect_fd = 0;
    }  
    close(socket_fd);  
    return 0;
}  

void _send_git_info(int connect_fd)
{

	const char sum_str[] = "Auto generated!\r\n";
	const char isolate_line[] = "==================================================================================\r\n";

	write(connect_fd, "\r\nBuild:", 8);
	write(connect_fd, _build_time, strlen(_build_time));
	write(connect_fd, "\r\nPath:", 7);
	write(connect_fd, _src_path, strlen(_src_path));
	write(connect_fd, "\r\n", 2);
	write(connect_fd, isolate_line, strlen(isolate_line));
	write(connect_fd, _git_branch, strlen(_git_branch));
	write(connect_fd, "\r\n", 2);
	write(connect_fd, _git_info, strlen(_git_info));
	write(connect_fd, sum_str, strlen(sum_str));
	return;
}
