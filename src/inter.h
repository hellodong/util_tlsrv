

#ifndef _INTER_H_
#define _INTER_H_

#include <stdint.h>

#define COUNTOF(x)  sizeof(x)/sizeof(x[0])


int inter_init(uint8_t in_out[], uint32_t out_size, const char start_name[]);
int inter_lineinput(uint8_t in_out[], uint32_t input_len, uint32_t out_size);


#endif
