


#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

typedef int (*fcn_cmd)(char *start_str, char *end_str, char sub_line_str[], uint8_t out[], uint32_t out_size);

typedef struct
{
	char *cmd_name;
	fcn_cmd cmd_process;
	char *des;
}stFstCmd_t;

int __echo_process(char *start_cmd, char *end_cmd, char *sub_line_str, uint8_t out[], uint32_t out_size);

int __exit_process(char *start_cmd, char *end_cmd, char *sub_line_str, uint8_t out[], uint32_t  out_size);


stFstCmd_t g_fstcmd[] = {
	{"echo", __echo_process, "just echo"},
	{"exit", __exit_process, "exit telnet"},
};

static int inputcmp(char *start_str, char *end_str, char *dest)
{
	if (strlen(dest) != end_str - start_str)
	{
		return 0;
	}

	while(start_str < end_str)
	{
		if (*start_str++ != *dest++)
		{
			return 0;
		}
	}
	return 1;
}


static fcn_cmd __cmd_find(char *start_str, char *end_str)
{
	uint32_t idx = 0;
	for (idx = 0;idx < sizeof(g_fstcmd)/sizeof(g_fstcmd[0]);idx++)	
	{
		if(inputcmp(start_str, end_str, g_fstcmd[idx].cmd_name))	
		{
			return g_fstcmd[idx].cmd_process;
		}
	}
	return NULL;
}

int _cmd_line_process(char line_str[], uint8_t out[], uint32_t out_size)
{
	int idx = 0;
	char *argv[2] = {NULL,NULL};
	char *sub_str = NULL;
	fcn_cmd process = NULL;

	// remove space at start first
	for (idx = 0; line_str[idx] != '\0' && line_str[idx] == ' ';idx++);

	argv[0] = line_str +idx;
	for (;line_str[idx] != '\0';idx++)
	{
		if (line_str[idx] == ' ')
		{
			argv[1] = line_str + idx; 
		}

		if(line_str[idx] == ' ' && line_str[idx+1] != ' ')	
		{
			sub_str = line_str + idx + 1;
			break;
		}
	}

	if (line_str[idx] == '\0')
	{
		argv[1] = line_str + idx;
	}

	if (argv[1] >  argv[0])
	{
		process=__cmd_find(argv[0], argv[1]);
		if (process)
		{
			return process(argv[0], argv[1], sub_str, out, out_size);
		}
	}

	return snprintf(out, out_size, "not find valid command\n");
}

int __echo_process(char *start_cmd, char *end_cmd, char *sub_line_str, uint8_t out[], uint32_t out_size)
{
	if (sub_line_str && strlen(sub_line_str))
	{
		return snprintf(out, out_size, "%s", sub_line_str);
	}
	out[0] = ' ';
	return 1;
}

int __exit_process(char *start_cmd, char *end_cmd, char *sub_line_str, uint8_t out[], uint32_t out_size)
{
	out[0] = 255;
	return 0;
}
