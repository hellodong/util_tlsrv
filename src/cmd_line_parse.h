

#ifndef _CMD_LINE_PARSE_H_
#define _CMD_LINE_PARSE_H_

#include <stdint.h>

int _cmd_line_process(char line_str[], uint8_t out[], uint32_t out_size);

#endif

